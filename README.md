# This product is not complete. (work-in-progress)

## maxttor.voucherpass

This is a Plone authentication mechanism that allow the user to login in to the System using a voucher code. The voucher-code is new content type.  The registered members can have one or more codes associated with them.
This plugin is independent and will not affect the standard Plone "username and password" plugin.  

Interesting use-cases for this product
 - for promotion and advertisement, it is possible to distribute a "trial access" for potential new clients.
 - if a user want to temporary give access to the system to another trusted-person with your credentials (in case of vacations for example).

The following configuration can be set for a voucher code:

**E-mail address required**  

The user must to inform his Email address with the voucher code. The E-mail will not be validate (for the moment), but will be registered in the voucher when it is used for the first time. 

**Number of access**

How many times this voucher can be used. Ever time the user authenticate, the number of access will be verified. 

**Validity-Days**

How many days this voucher will be valid, after the first use.

**Expiration-Date**

After this date the voucher is not valid anymore.

### Control panel

The product have the following configuration in the control panel.

**voucherpass enabled**

(checkbox enabled by default) - If possible to disable temporary the entire PAS plugin. The user will receive a warning "It was not possible to login. The voucher plugin is disabled at the moment".

**code length**

(minimum of 5) - How many chars will compose the voucher code

**code with special chars**

(checkbox disabled by default) - If this option is set, the voucher code could contain special chars like "%#@~", otherwise it will contain just letters, numbers or underscore.

**voucherpass _codes_fld**
For better performance, all Voucher codes are stored by default in the folder "/voucherpass_codes".


## Installation
Add to the instance section of your buildout:

eggs = ...
    pas.plugins.ldap

Run buildout. Restart Plone.
Then go to the Plone control-panel, select extensions and install the voucher Plugin.

## Views
The following views are available

**voucherpass_login**

Login page for the voucherpass. 

![voucherpass - Login view](docs/img/voucher_login.png)

** voucherpass **

A list view for all vouchers with a search field.

![voucherpass - Login view](docs/img/voucher_voucher_registry.png)


from AccessControl.Permissions import manage_users
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PluggableAuthService import registerMultiPlugin

import plugin

manage_add_voucherpass_form = PageTemplateFile('browser/add_plugin',
                            globals(), __name__='manage_add_voucherpass_form' )


def manage_add_voucherpass_helper( dispatcher, id, title=None, REQUEST=None ):
    """Add an voucherpass Helper to the PluggableAuthentication Service."""

    sp = plugin.VoucherpassHelper( id, title )
    dispatcher._setObject( sp.getId(), sp )

    if REQUEST is not None:
        REQUEST['RESPONSE'].redirect( '%s/manage_workspace'
                                      '?manage_tabs_message='
                                      'voucherpassHelper+added.'
                                      % dispatcher.absolute_url() )


def register_voucherpass_plugin():
    try:
        registerMultiPlugin(plugin.VoucherpassHelper.meta_type)
    except RuntimeError:
        # make refresh users happy
        pass


def register_voucherpass_plugin_class(context):
    context.registerClass(plugin.VoucherpassHelper,
                          permission = manage_users,
                          constructors = (manage_add_voucherpass_form,
                                          manage_add_voucherpass_helper),
                          visibility = None,
                          icon='browser/icon.gif'
                         )

## maxttor.voucherpass


### Install
The installation process must to execute the following:
 - install the profile with all elements: 
   - contentypes
   - properties, views, Control panel 
   - PAS Plugin
   - etc...
 - Add a new folder in the root with the name "voucherpass_codes" (private)
   - Add the Interface IVoucherPassContainer (Voucherpass container)  to this folder.
   - Configure the PAS property "voucherpass _codes_fld". 


### Content-type
Type:  Voucherpass
Permission: just Administrator 

fields
code - The voucher code is the primary key. It's not possible to register two vouchers with the same code. The system will generate a random voucher code for the user when the voucher is created. The user can also edit this code if and generate your own code.
required_email
number_acess - How many times this voucher was used
number_acess_max - How many times this voucher can be used
validity_days
date_expiration - Expiration date
date_first_use - Date when the code was used for the first time
date_last_use - Date when the code was used for the last time
username - The voucher is associate with this username. To register the username is necessary a widget able to search the members (username, Name or E-Mail). For this can be used the AutocompleteWidget (https://github.com/plone/plone.formwidget.autocomplete) or some better solution.

**Control-panel**
All the configurations  of the plugin must to be accessible from the control-panel (enabled, coe_length, etc...)

**Performance**
The login process is critical and the access to the DB to look for the voucher will take too much time. To solve this problem is necessary to read all voucher codes (just the codes) in the DB to the memory. Every time a voucher code is updated, the codes in the memory need to be updated.

### Views 
name: /voucherpass_login
registered for:  root

![voucherpass - Login view](docs/img/voucher_login.png)

 - Login page for the voucherpass with:
Voucher-code: (text)
Email: (text with e-mail Email address validator) E-mail address.   
Anonymous (checkbox) - if the user enable this option, the E-Mail address field will turn disabled (grey).

Direct Login Link

if the user inform code and e-mail in the query-string, the login must to be automatically, e.g.
site.com/login-voucher?code=A1B2C3&anonymous=1
site.com/login-voucher?code=A1B2C3&email=mymail@test.com

Voucher registry
name: /voucherpass
registered for:  IVoucherPassContainer Interface

![voucherpass - Login view](docs/img/voucher_voucher_registry.png)

A list view for all vouchers with a search field.
search - The fields  

### PAS Plugin

**Properties**

The configuration for the plugin will be stored in the properties of the PAS Plugin.
 - enabled (boolean)
 - code_length (min=5)
 - code_specialchars (boolean) = False
 - voucherpass _codes_fld = "/voucherpass _codes" - The Folder where the vouchercodes are stored.

The following verifications need to be executed in order to authenticate the user:
- check if the product is enabled
- check if the vouchercode is in the memory and get the voucher
- check voucher.required_email
  - Check if the user doesn't enabled "anonymous login"   
  - If there is no Email registered in this voucher, verify if the user informed a valid email address.   
  - If there is already a e-mail registered for this voucher, check the e-mail informed by the user.
- check if  voucher.number_access_max != 0 and (voucher.number_access < voucher.number_access_max)
- check if voucher.date_expiration != 0 and TODAY <= voucher.date_expiration
- check if voucher.validity_days != 0 and TODAY <=  voucher.date_first_use+voucher.validity_days
expiration

If the login was successful update the voucher:
- increase the number_access
- Register the new Email address if necessary
- Update date_first_use if necessary
- Update date_last_use

### Tests
**UnitTests**
 - test all functions with all possible parameters 
 - Test the install and reinstall process
**Integration testing**
All functions must to be tested - 100% coverage. e.g. 
- create, edit and delete Vouchers
- create a user and associate with one and more vouchers
 - test the login with all possible combination of configuration  (validity_days, expiration, etc)

### Requirements
Checklist
- All text is i18n compatible 
- pep8 conform
